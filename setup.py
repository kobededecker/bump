from distutils.core import setup
from setuptools import find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("version", "r") as vh:
    version = vh.read()

setup(
    name="Bump",
    author="Kobe De Decker",
    author_email="kobededecker@gmail.com",
    description="bump version",
    version=version,
    packages=find_packages(),
    install_requires=["click", "pyyaml", "pyinquirer"],
    url="",
    entry_points="""
        [console_scripts]
        bump=bump.main:bump
    """,
    license="MIT",
)
