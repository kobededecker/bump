from collections import defaultdict
import click
import yaml
import PyInquirer
import os, glob


def readversion():
    try:
        with open("version", "r") as versionfile:
            version = list(map(lambda x: int(x), versionfile.read().split(".")))
    except FileNotFoundError:
        version = "0.0.0"
    return version


def concatversion(version):
    return ".".join([str(x) for x in version])


def writeversion(version):
    with open("version", "w") as versionfile:
        versionfile.write(concatversion(version))


@click.group(chain=True, help="Helps managing IMDC release flow")
@click.pass_context
def bump(ctx):
    ctx.obj = {"version": readversion()}


@bump.command(help="Initialize repository for use with Bump")
def init():
    style = PyInquirer.style_from_dict(
        {
            PyInquirer.Token.QuestionMark: "#E91E63 bold",
            PyInquirer.Token.Selected: "#673AB7 bold",
            PyInquirer.Token.Instruction: "",  # default
            PyInquirer.Token.Answer: "#2196f3 bold",
            PyInquirer.Token.Question: "",
        }
    )
    questions = [
        {
            "type": "checkbox",
            "name": "includes",
            "choices": [
                {"checked": True, "name": "release.bat"},
                {"checked": True, "name": "version"},
                {"checked": False, "name": "CHANGELOG.md"},
                {"checked": False, "name": "Docker files"},
            ],
            "message": "Which files to include?",
        },
    ]
    answers = PyInquirer.prompt(questions, style=style)
    for i in answers["includes"]:
        if i == "release.bat":
            with open("release.bat", "w") as f:
                f.write(
                    """bump merge %1 && git add . && git commit -m "release v%1" && git tag -f %1 && git push && git push --tags"""
                )
        elif i == "version":
            with open("version", "w") as f:
                f.write("0.0.0")
        elif i == "CHANGELOG.md":
            with open("CHANGELOG.md", "w") as f:
                f.write("## 0.0.0\n\n\t- initialized repository")
        elif i == "Docker files":
            Dockerfile = PyInquirer.prompt(
                [
                    {
                        "type": "list",
                        "choices": ["Django", "Vue"],
                        "name": "type",
                        "message": "Which kind of project are you working on?",
                    }
                ],
                style=style,
            )
            if Dockerfile["type"] == "Django":
                with open("Dockerfile", "w") as f:
                    f.write(
                        """FROM python:3
# ENVS
ENV SECRET_KEY=iic%)+@!+st&9t0l7$lg7r@f=dheh3#ywy-lq-+qul9$b5hj9)

EXPOSE 8000

WORKDIR /app
COPY requirements.txt /app

# Using pip:
RUN apt-get install -y libpq-dev
RUN python3 -m pip install -r requirements.txt

ADD . /app
RUN python manage.py collectstatic --noinput
ENTRYPOINT gunicorn netlink.wsgi:application -b 8000"""
                    )
            elif Dockerfile["type"] == "Vue":
                with open("Dockerfile", "w") as f:
                    f.write(
                        """FROM nginx
COPY dist /usr/share/nginx/html"""
                    )



@bump.command(help="Add an element for the changelog")
@click.argument("input", type=click.STRING, nargs=-1)
def add(input):
    style = PyInquirer.style_from_dict(
        {
            PyInquirer.Token.QuestionMark: "#E91E63 bold",
            PyInquirer.Token.Selected: "#673AB7 bold",
            PyInquirer.Token.Instruction: "",  # default
            PyInquirer.Token.Answer: "#2196f3 bold",
            PyInquirer.Token.Question: "",
        }
    )
    questions = [
        {
            "type": "input",
            "name": "title",
            "default": " ".join(input),
            "message": "Changelog message?",
        },
        {"type": "input", "message": "Merge Request ID", "name": "mr"},
        {
            "type": "list",
            "name": "type",
            "message": "Type",
            "choices": [
                "Added",
                "Fixed",
                "Changed",
                "Deprecated",
                "Removed",
                "Security",
                "Performance",
                "Other",
            ],
        },
    ]
    answers = PyInquirer.prompt(questions, style=style)

    try:
        os.mkdirs("changelogs/unreleased")
    except:
        pass
    with open(
        os.path.join("changelogs", "unreleased", answers["title"] + ".yml"), "w"
    ) as fileke:
        fileke.write(yaml.dump(answers))


@bump.command(help="Merge changelogs entry and inject in changelog")
@click.argument("version")
def merge(version):
    msgs = {}
    ls = glob.glob(os.path.join("changelogs/unreleased", "*.yml"))
    for i in ls:
        with open(i, "r") as f:
            obj = yaml.safe_load(f.read())
            if obj["type"].lower() not in msgs:
                msgs[obj["type"].lower()] = []
            msgs[obj["type"].lower()].append(obj)

    with open("CHANGELOG.md", "r") as changelog:
        original = changelog.read()
    with open("CHANGELOG.md", "w") as changelog:
        forprint = f"## {version}"
        for i in msgs:
            forprint += f"\n\n### {i.capitalize()}\n\n"
            for j in msgs[i]:
                forprint += f"- {j['title']}"
                if j.get("author", None) or j.get("mr", None):
                    forprint += " ("
                    if j.get("author", None):
                        forprint += f" @{j.get('author')} "
                    if j.get("mr", None):
                        forprint += f" !{j.get('mr') }"
                    forprint += ")"
                forprint += "\n"
        #     msgs = "\n\t".join(["- " + i["title"] for i in msgs])
        #     newmsg = f"""# {version}
        # {msgs}"""
        changelog.write(forprint + "\n\n" + original)

    for i in ls:
        os.remove(i)


@bump.command(help="Add a major increment to the version number")
@click.pass_obj
def major(obj):
    obj["version"][0] += 1
    obj["version"][1] = 0
    obj["version"][2] = 0
    click.echo("New version: %s" % concatversion(obj["version"]))

    writeversion(obj["version"])


@bump.command(help="Add a minor increment to the version number")
@click.pass_obj
def minor(obj):
    obj["version"][1] += 1
    obj["version"][2] = 0
    click.echo("New version: %s" % concatversion(obj["version"]))
    writeversion(obj["version"])


@bump.command(help="Add a patch increment to the version number")
@click.pass_obj
def patch(obj):
    obj["version"][2] += 1
    click.echo("New version: %s" % concatversion(obj["version"]))

    writeversion(obj["version"])


@bump.command()
@click.pass_obj
def release(obj, help="call the release.bat script with the curren version number"):
    import subprocess

    subprocess.check_output(["release.bat", "%s" % concatversion(obj["version"])])

@bump.command()
def clean():
    logs = []
    obj = {}
    action = None
    items = []
    with open("CHANGELOG.md", "r") as f:
        for line in f:
            if line.startswith("## "):
                if obj != {}:
                    if action:
                        obj[action] = items
                        action = None
                        items = []
                    logs.append(obj)
                    obj = {}
                obj["version"] = line.split(" ")[-1].strip()
            elif line.startswith("### "):
                if action:
                    obj[action] = items
                    items = []
                action = line.split(" ")[1].strip()
            elif line.strip().startswith("-"):
                item = line.strip()[2:].strip()
                if item: 
                    items.append(item)

    with open("CHANGELOG.md", "w") as f:
        for obj in logs:
            version = obj.pop("version")
            if obj!={}:
                f.write(f"## {version}\n\n")

                for i in obj:
                    f.write(f"### {i} " + (f"({len(obj[i])} changes)" if len(obj[i]) >1 else "") + f"\n\n")
                    for j in obj[i]:
                        f.write(f" - {j}\n")
                    f.write("\n")
                f.write("\n")